﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collapseRing : MonoBehaviour
{

    public int countdown;
    Animator anim;
    public Transform nextRing;

    // Start is called before the first frame update
    void Start()
    {
        float secs = countdown * 60.0f;
        anim = GetComponent<Animator>();
        Invoke("StartCollapse", secs);
        Debug.Log("Current Value of " + this.name + " should activate after " + secs + " seconds");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Collapse() 
    {   
        Transform buildNext = Instantiate(nextRing, this.transform.position, this.transform.rotation);
        buildNext.localScale = new Vector3(100, 100, 80);
        if (buildNext.gameObject.GetComponent<collapseRing>() != null) 
        {
            buildNext.gameObject.GetComponent<collapseRing>().countdown = 0;
        }


        Destroy(this.gameObject);
    }

    void StartCollapse() 
    {
        anim.SetTrigger("Start Collapse");
        Invoke("Collapse", 60.0f);
    }
}
