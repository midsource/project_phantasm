﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float damage;
    public float lifetime;
    public GameObject hitFX;

    void Start()
    {
        lifetime = 2.0f;
    }

    void Update()
    {
        lifetime -= Time.deltaTime;
        if (lifetime < 0)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (hitFX != null) { Instantiate(hitFX, transform.position, Quaternion.identity); }
        if (col.transform.gameObject.tag == "Player")
        {
            col.transform.gameObject.GetComponent<StatusManager>().TakeDamage(damage);
            Destroy(gameObject);
        }
    }
}
