﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class Data
{
    public int scene;
    public float HP;
    public float currTime;
    public float[] position;

    public Data(StatusManager status) {
        scene = SceneManager.GetActiveScene().buildIndex;
        HP = status.HP;
        currTime = status.currTime;
        
        position = new float[3];
        position[0] = GameObject.FindWithTag("Player").transform.position.x;
        position[1] = GameObject.FindWithTag("Player").transform.position.y;
        position[2] = GameObject.FindWithTag("Player").transform.position.z;
    }
}
