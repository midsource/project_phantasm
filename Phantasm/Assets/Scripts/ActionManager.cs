﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;
using UnityEngine.AI;

public class ActionManager : MonoBehaviour
{
    // Slowdown variables
    public float slowdown;
    public bool slowed;
    public bool canMenu;
    public bool canOpen;

    // Camera control
    public CinemachineFreeLook vcam;
    public CinemachineFreeLook.Orbit[] orbits;
    public GameObject aim;
    public GameObject cam;
    public GameObject zoomCam;
    public Vector3 normalOffset;
    public Vector3 revolverOffset;

    // UI control
    public bool inDialogue;
    public bool quickActivation;
    public GameObject ActionRevolver;
    public Quaternion ringRotation;
    protected RingMenu RevolverInstance;
    public SelectedIcon selectedIcon;
    public GameObject aimPreview;
    public Image crosshair;
    private GameObject cylinder;

    // Sound control
    public AudioSource a_s;
    public AudioClip oneCooldown;
    public AudioClip twoCooldown;
    public AudioClip threeCooldown;
    public AudioClip openClip;
    public AudioClip fireClip;
    public AudioClip warpClip;
    public AudioClip delugeClip;
    public AudioClip venomClip;

    // Action Control
    public GameObject player;
    public ThirdPersonMovement controller;
    public Animator anim;
    public enum ActionState { None, Firing, Teleporting, Deluging, Inferno, Barrier, Venom };
    public ActionState actionState;
    public string slottedAction;
    public int shootingDamage;
    public Transform cursorAim;
    public Transform revolver_t;
    public float cooldown;
    public float dcooldown;
    private bool actionReady = false;

    // Special control
    public GameObject positionCylinder;
    public GameObject ybotClone;
    public GameObject delugeArrow;
    public GameObject delugeEffect;
    public GameObject teleportEffect;
    public GameObject infernoCircle;
    public GameObject infernoEffect;
    public GameObject barrierCylinder;
    public GameObject fakeBarrier;
    public GameObject trueBarrier;
    public GameObject venomCylinder;

    public Transform delugeAnchor;

    private GameObject ybotPreview;
    private GameObject positionPreview;
    private GameObject delugePreview;
    private GameObject infernoPreview;
    private GameObject placePreview;
    private GameObject barrierPreview;
    private GameObject venomPreview;

    // Level control
    public int evidenceFound;
    

    // -------------------------------------------------
    // STARTER FUNCTIONS
    // -------------------------------------------------

    void Start()
    {
        inDialogue = false;
        slowdown = 0.01f;
        slowed = false;
        canMenu = true;
        quickActivation = false;
        evidenceFound = 0;
        actionReady = false;
        dcooldown = 0;

        // ActionRevolver = GameObject.Find("ActionRevolver");
        shootingDamage = 10;

        aim = GameObject.Find("aim_transform");
        cursorAim = GameObject.Find("AimingNode").transform;
        revolver_t = GameObject.Find("BlasterAim").transform;
        cam = GameObject.Find("ThirdPersonCamera");
        delugeAnchor = GameObject.Find("delugeAnchor").transform;

        slottedAction = "Interact";
        selectedIcon = gameObject.GetComponent<SelectedIcon>();

        player = GameObject.FindWithTag("Player");
        controller = FindObjectOfType<ThirdPersonMovement>();
        anim = player.GetComponent<Animator>();
        a_s = GetComponent<AudioSource>();

        crosshair = GameObject.Find("SimpleCrosshair").GetComponent<Image>();
        vcam = GameObject.Find("ThirdPersonCamera").GetComponent<CinemachineFreeLook>();
        orbits = vcam.m_Orbits;
        vcam.GetComponent<CinemachineCameraOffset>().m_Offset = normalOffset;

        if (GameObject.FindWithTag("MainCamera").GetComponent<AudioSource>()) {
            GameObject.FindWithTag("MainCamera").GetComponent<AudioSource>().volume *= PlayerPrefs.GetFloat("VolumeModifier");
        } else if (GameObject.Find("EventSystem").GetComponent<AudioSource>()) {
            GameObject.Find("EventSystem").GetComponent<AudioSource>().volume *= PlayerPrefs.GetFloat("VolumeModifier");
        }
    }

    void Update()
    {
        // If you're disabled or in hitstun, you can't use the Action Revolver
        if (controller.disabled) { return; }

        // Cooldown functionality
        if (cooldown > 0 && !slowed)
        {
            cooldown -= Time.deltaTime;
            GameObject.Find("SelectedIcon").GetComponent<CanvasGroup>().alpha = 0.1f;
        } else {
            GameObject.Find("SelectedIcon").GetComponent<CanvasGroup>().alpha = 1f;
        }

        if (dcooldown > 0 && !slowed) {
            dcooldown -= Time.deltaTime;
        }


        // ---------
        // CURSOR CHANGING BASED ON TARGET
        // ---------
        Ray check = new Ray(aim.transform.position, cursorAim.position - aim.transform.position);
        RaycastHit checkInfo;

        if (Physics.Raycast(check, out checkInfo, 3))
        {
            if (checkInfo.collider.GetComponent<Interactable>() && cooldown < 0.01f)
            {
                crosshair.color = Color.green;
            } else if (checkInfo.collider.GetComponent<AnalysisTarget>() && cooldown < 0.01f) {
                crosshair.color = Color.yellow;
            } else
            {
                crosshair.color = Color.white;
            }
        } else
        {
            crosshair.color = Color.white;
        }

        if (Physics.Raycast(check, out checkInfo, 50))
        {
            if ((checkInfo.collider.GetComponent<EnemyHP>() || checkInfo.collider.GetComponent<Destructible>()) && (cooldown < 0.01f || cylinder != null))
            {
                crosshair.color = Color.red;
            }
        }

        // Quick step
        if (Input.GetKey (KeyCode.LeftShift))
        {
            if (dcooldown < 0.01 && !slowed) {
                dcooldown = 2;

                Instantiate(teleportEffect, player.transform.position, Quaternion.identity);
                player.GetComponent<CharacterController>().enabled = false;

                Ray r = new Ray(player.transform.position, player.transform.forward);
                RaycastHit hitInfo;

                if (Physics.Raycast(r, out hitInfo, 5))
                {
                    player.transform.position += player.transform.forward * hitInfo.distance;
                } else {
                    player.transform.position += player.transform.forward * 5;
                }

                player.GetComponent<CharacterController>().enabled = true;
                Instantiate(teleportEffect, player.transform.position, Quaternion.identity, player.transform);

                a_s.PlayOneShot(warpClip, PlayerPrefs.GetFloat("VolumeModifier"));
            }
        }

        // Interaction/Analysis
        if (Input.GetKey (KeyCode.E))
        {
            Interact();
            Analyze();
        }

            // ===========================
            // LEFT CLICK ACTIONS
            // ===========================
            if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Action State: " + actionState + " || Action Ready: " + actionReady);
            if (canMenu == true && !GameObject.FindWithTag("Ring") && cooldown <= 0)
            {
                // StartCoroutine(ReturnAnim());
                quickActivation = true;
                // QuickFire();
                UseAction(slottedAction);
            }
            // ---------
            // TIME WARP
            // ---------
            else if (actionState == ActionState.Teleporting && actionReady)
            {
                Debug.Log(ybotPreview.transform.position);
                player.GetComponent<CharacterController>().enabled = false;
                Instantiate(teleportEffect, player.transform.position, Quaternion.identity);
                player.transform.position = ybotPreview.transform.position + new Vector3(0, 0.5f, 0);
                Instantiate(teleportEffect, player.transform.position, Quaternion.identity, player.transform);
                player.GetComponent<CharacterController>().enabled = true;

                a_s.PlayOneShot(warpClip, PlayerPrefs.GetFloat("VolumeModifier"));
                a_s.PlayOneShot(twoCooldown, PlayerPrefs.GetFloat("VolumeModifier"));

                GameObject.Destroy(positionPreview);
                GameObject.Destroy(ybotPreview);

                controller.anim.speed = 1;
                actionState = ActionState.None;
                quickActivation = false;

                ReturnTime();
            } 
            // ---------
            // FIRE
            // ---------
            else if (actionState == ActionState.Firing && actionReady)
            {
                Ray r = new Ray(aim.transform.position, cursorAim.position - aim.transform.position);
                RaycastHit hitInfo;

                if (Physics.Raycast(r, out hitInfo, 100))
                {
                    var aenemy = hitInfo.collider.GetComponent<EnemyHP>();
                    var destructible = hitInfo.collider.GetComponent<Destructible>();
                    if (aenemy)
                    {
                        aenemy.TakeDamage(shootingDamage);
                    }
                    else if (destructible)
                    {
                        destructible.Destroy();
                    }
                }
                a_s.PlayOneShot(fireClip, 0.3f * PlayerPrefs.GetFloat("VolumeModifier"));
                GameObject.Destroy(cylinder);
                a_s.PlayOneShot(twoCooldown, PlayerPrefs.GetFloat("VolumeModifier"));

                controller.anim.speed = 1;
                actionState = ActionState.None;
                quickActivation = false;

                ReturnTime();
            }
            // ---------
            // DELUGE
            // ---------
            else if (actionState == ActionState.Deluging && actionReady)
            {
                int layerMask = 1 << 11;
                Collider[] hits = Physics.OverlapSphere(delugeAnchor.position, 10.0f, layerMask);
                foreach (var hit in hits)
                {
                    if (hit.gameObject.GetComponent<Rigidbody>())
                    {
                        var enemyRB = hit.gameObject.GetComponent<Rigidbody>();
                        var enemyNMA = hit.gameObject.GetComponent<NavMeshAgent>();
                        var enemyHP = hit.gameObject.GetComponent<EnemyHP>();
                        var moveDir = aim.transform.position - hit.gameObject.transform.position;

                        Debug.Log(hit.gameObject.name);
                        if (enemyNMA)
                            enemyNMA.enabled = false;

                        if (enemyHP)
                            enemyHP.TakeDamage(shootingDamage/2);

                        if (hit.gameObject.GetComponent<Animator>()) hit.gameObject.GetComponent<Animator>().applyRootMotion = false;
                        enemyRB.isKinematic = false;
                        enemyRB.AddForce(moveDir.normalized * -20f, ForceMode.Impulse);
                    }
                }

                
                a_s.PlayOneShot(delugeClip, PlayerPrefs.GetFloat("VolumeModifier"));
                Instantiate(delugeEffect, delugePreview.transform.position, delugePreview.transform.rotation);
                GameObject.Destroy(delugePreview);
                a_s.PlayOneShot(twoCooldown, PlayerPrefs.GetFloat("VolumeModifier"));

                controller.anim.speed = 1;
                actionState = ActionState.None;
                quickActivation = false;

                ReturnTime();
            }
            // ---------
            // INFERNO
            // ---------
            else if (actionState == ActionState.Inferno && actionReady) {
                Instantiate(infernoEffect, infernoPreview.transform.position, Quaternion.identity);
                GameObject.Destroy(infernoPreview);
                a_s.PlayOneShot(threeCooldown, PlayerPrefs.GetFloat("VolumeModifier"));

                controller.anim.speed = 1;
                actionState = ActionState.None;
                quickActivation = false;

                ReturnTime();
            // ---------
            // BARRIER
            // ---------
            } else if (actionState == ActionState.Barrier && actionReady) {
                if (GameObject.Find("BarrierCube(Clone)")) {
                    GameObject.Destroy(GameObject.Find("BarrierCube(Clone)"));
                }
                Instantiate(trueBarrier, barrierPreview.transform.position, barrierPreview.transform.rotation);
                GameObject.Destroy(placePreview);
                GameObject.Destroy(barrierPreview);
                a_s.PlayOneShot(twoCooldown, PlayerPrefs.GetFloat("VolumeModifier"));

                actionState = ActionState.None;
                quickActivation = false;

                ReturnTime();
            // ---------
            // VENOM
            // ---------
            } else if (actionState == ActionState.Venom && actionReady) {
                Ray r = new Ray(aim.transform.position, cursorAim.position - aim.transform.position);
                RaycastHit hitInfo;

                if (Physics.Raycast(r, out hitInfo, 100))
                {
                    var aenemy = hitInfo.collider.GetComponent<EnemyHP>();
                    var enemy = hitInfo.collider.GetComponent<Enemy>();
                    var destructible = hitInfo.collider.GetComponent<Destructible>();
                    if (aenemy)
                    {
                        aenemy.TakeDamage(0);
                    }
                    else if (destructible)
                    {
                        destructible.Destroy();
                    }

                    if (enemy) {
                        enemy.Stun();
                    }
                }
                a_s.PlayOneShot(venomClip, 0.3f * PlayerPrefs.GetFloat("VolumeModifier"));
                GameObject.Destroy(venomPreview);
                a_s.PlayOneShot(twoCooldown, PlayerPrefs.GetFloat("VolumeModifier"));

                controller.anim.speed = 1;
                actionState = ActionState.None;
                quickActivation = false;

                ReturnTime();
            }
        }


        // ===========================
        // ACTION REVOLVER OPENING
        // ===========================
        if (Input.GetMouseButtonDown(1) && cooldown <= 0 && canOpen)
        {
            if (slowed && canMenu) {
                if (ActionRevolver != null) {
                    ringRotation = ActionRevolver.transform.localRotation;
                    GameObject.Find("ActionInfo").GetComponent<CanvasGroup>().alpha = 0;
                    ActionRevolver.SetActive(false);
                }
                ReturnTime();
            } else if (canMenu) {
                a_s.PlayOneShot(openClip, PlayerPrefs.GetFloat("VolumeModifier"));
                // RevolverInstance = Instantiate(ActionRevolver, GameObject.Find("MainCanvas").transform);
                // RevolverInstance.callback = MenuClick;
                GameObject.Find("ActionInfo").GetComponent<CanvasGroup>().alpha = 1;
                ActionRevolver.GetComponent<RingMenu>().callback = MenuClick;
                ActionRevolver.SetActive(true);
                SlowTime(); 
            }
        } else if (Input.GetMouseButtonDown(1) && actionState != ActionState.None && !quickActivation) {
            switch (actionState) {
                    case ActionState.Firing:
                        Destroy(cylinder);
                        break;
                    case ActionState.Deluging:
                        Destroy(delugePreview);
                        break;
                    case ActionState.Teleporting:
                        Destroy(ybotPreview);
                        Destroy(positionPreview);
                        break;
                    case ActionState.Inferno:
                        Destroy(infernoPreview);
                        break;
                    case ActionState.Barrier:
                        Destroy(placePreview);
                        Destroy(barrierPreview);
                        break;
                    case ActionState.Venom:
                        Destroy(venomPreview);
                        break;
                }
                controller.anim.SetTrigger("animCancel");
                controller.anim.speed = 1;
                actionState = ActionState.None;
                cooldown = 0f;
                canMenu = true;

                ActionRevolver.SetActive(true);
        } else if  (Input.GetMouseButtonDown(1) && actionState != ActionState.None && quickActivation) {
            switch (actionState) {
                case ActionState.Firing:
                    Destroy(cylinder);
                    break;
                case ActionState.Deluging:
                    Destroy(delugePreview);
                    break;
                case ActionState.Teleporting:
                    Destroy(ybotPreview);
                    Destroy(positionPreview);
                    break;
                case ActionState.Inferno:
                    Destroy(infernoPreview);
                    break;
                case ActionState.Barrier:
                    Destroy(placePreview);
                    Destroy(barrierPreview);
                    break;
                case ActionState.Venom:
                    Destroy(venomPreview);
                    break;
            }
            controller.anim.SetTrigger("animCancel");
            controller.anim.speed = 1;
            actionState = ActionState.None;
            cooldown = 0f;
            quickActivation = false;

            ReturnTime();
        }

        if (slowed)
        {
            // Slow down world
            Time.timeScale = Mathf.Clamp(Time.unscaledDeltaTime - 0.5f * Time.unscaledDeltaTime, slowdown, 1.0f);
            Time.fixedDeltaTime = Time.timeScale * 0.02f;
        }
        else
        {
            // Speed up world
            Time.timeScale += 1f * Time.unscaledDeltaTime;
            Time.fixedDeltaTime = Time.timeScale * 0.02f;
            Time.timeScale = Mathf.Clamp(Time.timeScale, slowdown, 1.0f);
        }

        // FOR FIRING PREVIEW
        if (cylinder != null)
        {
            var offset = cursorAim.position - revolver_t.position;
            var scale = new Vector3(0.05f, offset.magnitude / 2.0f, 0.05f);
            var position = revolver_t.position + (offset / 2.0f);

            cylinder.transform.position = revolver_t.position + (offset / 2.0f);
            cylinder.transform.up = offset;
            cylinder.transform.localScale = scale;
        }

        // FOR TIME WARP PREVIEW
        if (positionPreview != null)
        {
            var target = new Vector3(cursorAim.position.x, player.transform.position.y-0.8f, cursorAim.position.z);
            if (Vector3.Distance(player.transform.position, target) > 25) {
                target = player.transform.position + (target - player.transform.position).normalized * 25;
            }

            Ray r = new Ray(aim.transform.position, cursorAim.position - aim.transform.position);
            RaycastHit hitInfo;

            if (Physics.Raycast(r, out hitInfo, Vector3.Distance(player.transform.position, target)))
            {
                target = player.transform.position + (target - player.transform.position).normalized * hitInfo.distance;
            }

            var offset = target + new Vector3(0, 1.8f, 0) - aim.transform.position;
            var scale = new Vector3(0.1f, offset.magnitude / 2.0f, 0.1f);
            var position = aim.transform.position + (offset / 2.0f);

            positionPreview.transform.position = aim.transform.position + (offset / 2.0f);
            positionPreview.transform.up = offset;
            positionPreview.transform.localScale = scale;

            ybotPreview.transform.position = target;
            ybotPreview.transform.rotation = player.transform.rotation;
        }

        // FOR DELUGE PREVIEW
        if (delugePreview != null)
        {
            delugePreview.transform.position = aim.transform.position - new Vector3(0, 0.7f, 0) + aim.transform.forward*1.2f;
            delugePreview.transform.rotation = player.transform.rotation;
        }

        // FOR INFERNO PREVIEW
        if (infernoPreview != null)
        {
            var target = new Vector3(cursorAim.position.x, player.transform.position.y, cursorAim.position.z);
            if (Vector3.Distance(player.transform.position, target) > 25) {
                target = player.transform.position + (target - player.transform.position).normalized * 25;
            } 
            infernoPreview.transform.position = target;
        }

        // FOR BARRIER PREVIEW
        if (barrierPreview != null)
        {
            var target = new Vector3(cursorAim.position.x, player.transform.position.y + 1.0f, cursorAim.position.z);
            if (Vector3.Distance(player.transform.position, target) > 22) {
                target = player.transform.position + (target - player.transform.position).normalized * 22;
            }

            Ray r = new Ray(aim.transform.position, cursorAim.position - aim.transform.position);
            RaycastHit hitInfo;

            if (Physics.Raycast(r, out hitInfo, Vector3.Distance(player.transform.position, target)))
            {
                target = player.transform.position + (target - player.transform.position).normalized * (hitInfo.distance-1.2f);
            }

            var offset = target - aim.transform.position;
            var scale = new Vector3(0.1f, offset.magnitude / 2.0f, 0.1f);
            var position = aim.transform.position + (offset / 2.0f);

            placePreview.transform.position = aim.transform.position + (offset / 2.0f);
            placePreview.transform.up = offset;
            placePreview.transform.localScale = scale;

            barrierPreview.transform.position = target;
            barrierPreview.transform.rotation = player.transform.rotation;
        }

        // FOR VENOM PREVIEW
        if (venomPreview != null) {
            var offset = cursorAim.position - revolver_t.position;
            var scale = new Vector3(0.05f, offset.magnitude / 2.0f, 0.05f);
            var position = revolver_t.position + (offset / 2.0f);

            venomPreview.transform.position = revolver_t.position + (offset / 2.0f);
            venomPreview.transform.up = offset;
            venomPreview.transform.localScale = scale;
        }
    }

    private void MenuClick(string path)
    {
        // Debug.Log(path);
        var paths = path.Split('/');
    }

    // -------------------------------------------------
    // ACTION REVOLVER / TIME SLOW FUNCTIONS
    // -------------------------------------------------

    public void SlowTime()
    {
        controller.maxSpeed = 0f;
        controller.turnTime *= 50;
        controller.aiming = true;

        vcam.GetComponent<CinemachineCameraOffset>().m_Offset = revolverOffset;

        slowed = true;
    }

    public void ReturnTime()
    {
        canMenu = true;
        Time.timeScale = 1f;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;

        controller.maxSpeed = 6f;
        controller.turnTime /= 50;
        controller.aiming = false;

        actionReady = false;
        vcam.GetComponent<CinemachineCameraOffset>().m_Offset = normalOffset;
        slowed = false;
    }

    // -------------------------------------------------
    // ACTION CONTROL
    // -------------------------------------------------

    public void UseAction(string action)
    {
        if (cooldown > 0) { return; }
        switch(action)
        {
            case "Fire":
                Fire();
                actionState = ActionState.Firing;
                cooldown = 2f;
                break;
            case "Interact":
                Interact();
                cooldown = 0.1f;
                break;
            case "Analyze":
                Analyze();
                cooldown = 0;
                break;
            case "Time Warp":
                TimeWarp();
                actionState = ActionState.Teleporting;
                dcooldown = 2f;
                cooldown = 2f;
                break;
            case "Deluge":
                Deluge();
                actionState = ActionState.Deluging;
                cooldown = 2f;
                break;
            case "Inferno":
                Inferno();
                actionState = ActionState.Inferno;
                cooldown = 3f;
                break;
            case "Barrier":
                Barrier();
                actionState = ActionState.Barrier;
                cooldown = 2f;
                break;
            case "Venom":
                Venom();
                actionState = ActionState.Venom;
                cooldown = 2f;
                break;
            default:
                Debug.Log("Well uhhh... I don't think " + action + " is an action?");
                break;
        }
    }


    public void Fire()
    {
        if (cylinder == null)
        {
            controller.anim.SetTrigger("aim");
            canMenu = false;

            if (!slowed)
            {
                SlowTime();
            }
        }
    }

    public void Venom()
    {
        if (cylinder == null)
        {
            controller.anim.SetTrigger("aim");
            canMenu = false;

            if (!slowed)
            {
                SlowTime();
            }
        }
    }

    public void Aimed()
    {
        controller.anim.speed = 0;
        switch (slottedAction)
        {
            case "Fire":
                var offset = cursorAim.position - aim.transform.position;
                var scale = new Vector3(0.05f, offset.magnitude / 2.0f, 0.05f);
                var position = aim.transform.position + (offset / 2.0f);

                cylinder = Instantiate(aimPreview, position, Quaternion.identity);
                cylinder.transform.up = offset;
                cylinder.transform.localScale = scale;
                break;
            case "Deluge":
                position = aim.transform.position - new Vector3(0, 1f, 0) + aim.transform.forward;
                delugePreview = Instantiate(delugeArrow, position, player.transform.rotation);
                break;
            case "Inferno":
                infernoPreview = Instantiate(infernoCircle, cursorAim.position - new Vector3(0, 1f, 0), Quaternion.identity);
                infernoPreview.transform.rotation = player.transform.rotation;
                break;
            case "Venom":
                offset = cursorAim.position - aim.transform.position;
                scale = new Vector3(0.05f, offset.magnitude / 2.0f, 0.05f);
                position = aim.transform.position + (offset / 2.0f);

                venomPreview = Instantiate(venomCylinder, position, Quaternion.identity);
                venomPreview.transform.up = offset;
                venomPreview.transform.localScale = scale;
                break;
            default:
                Debug.Log("I think I'm just gonna do nothing here");
                break;
        }  
        actionReady = true;
    }

        public void Interact()
    {
        // Debug.Log("Interact!");
        /*
        int layerMask = LayerMask.GetMask("Interactable");
        Collider[] entities = Physics.OverlapSphere(delugeAnchor.position, 2f, layerMask);
        if (entities.Length == 0) { return; }
        foreach (var entity in entities)
        {
            if (entity.gameObject.GetComponent<Interactable>())
            {
                // Debug.Log(entity.gameObject.name);
                entity.gameObject.GetComponent<Interactable>().Interact();
            }
        }
        */

        Ray r = new Ray(aim.transform.position, cursorAim.position - aim.transform.position);
        RaycastHit hitInfo;

        if (Physics.Raycast(r, out hitInfo, 100))
        {
            var inter = hitInfo.collider.GetComponent<Interactable>();
            if (inter)
            {
                inter.Interact();
            }
        }
    }

    public void Analyze()
    {
        Debug.Log("Analyze!");
        Ray r = new Ray(aim.transform.position, cursorAim.position - aim.transform.position);
        RaycastHit hitInfo;

        if (Physics.Raycast(r, out hitInfo, 100))
        {
            var inter = hitInfo.collider.GetComponent<AnalysisTarget>();
            if (inter)
            {
                inter.Analyze();
            }
        }
    }

    public void TimeWarp()
    {
        var offset = cursorAim.position + new Vector3(0, -0.4f, 0) - aim.transform.position;
        var scale = new Vector3(0.1f, offset.magnitude / 2.0f, 0.1f);
        var position = aim.transform.position + (offset / 2.0f);

        positionPreview = Instantiate(positionCylinder, position, Quaternion.identity);
        positionPreview.transform.up = offset;
        positionPreview.transform.localScale = scale;

        ybotPreview = Instantiate(ybotClone, cursorAim.position - new Vector3(0, 1f, 0), Quaternion.identity);
        ybotPreview.transform.rotation = player.transform.rotation;

        actionReady = true;
        canMenu = false;
        if (!slowed)
        {
            SlowTime();
        }
    }

    public void Inferno()
    {
        controller.anim.SetTrigger("aim");
        canMenu = false;

        if (!slowed)
        {
            SlowTime();
        }
    }

    public void Deluge()
    {
        controller.anim.SetTrigger("aim");

        canMenu = false;
        if (!slowed)
        {
            SlowTime();
        }
    }

    public void Barrier()
    {
        var offset = cursorAim.position + new Vector3(0, -0.4f, 0) - aim.transform.position;
        var scale = new Vector3(0.1f, offset.magnitude / 2.0f, 0.1f);
        var position = aim.transform.position + (offset / 2.0f);

        placePreview = Instantiate(barrierCylinder, position, Quaternion.identity);
        placePreview.transform.up = offset;
        placePreview.transform.localScale = scale;

        barrierPreview = Instantiate(fakeBarrier, cursorAim.position - new Vector3(0, 1f, 0), Quaternion.identity);
        barrierPreview.transform.rotation = player.transform.rotation;

        actionReady = true;
        canMenu = false;
        if (!slowed)
        {
            SlowTime();
        }
    }

    public IEnumerator ReturnAnim()
    {
        yield return new WaitForSeconds(0.01f);
        controller.anim.speed = 1;
    }
}
