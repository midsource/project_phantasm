﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DialogueManager : MonoBehaviour
{
	public Text nameText;
	public Text dialogueText;
	public Dialogue currentDialogue;
	public bool isRunning;
	public bool isTyping;
	public bool skipDialogue;

	public AudioSource _as;
	public AudioClip blip;

	private float tempSpeed;

	public ActionManager am;

	public Animator animator;

	private Queue<DialogueLine> lines;

	// Use this for initialization
	void Start()
	{
		lines = new Queue<DialogueLine>();
		isTyping = false;
		skipDialogue = false;

		_as = GetComponent<AudioSource>();
		am = FindObjectOfType<ActionManager>();
		animator = GameObject.Find("DialogueBox").GetComponent<Animator>();

		nameText = GameObject.Find("NameText").GetComponent<Text>();
		dialogueText = GameObject.Find("MessageText").GetComponent<Text>();

		if (GameObject.FindWithTag("MainCamera").GetComponent<AudioSource>()) {
            GameObject.FindWithTag("MainCamera").GetComponent<AudioSource>().volume *= PlayerPrefs.GetFloat("VolumeModifier");
        } else if (GameObject.Find("EventSystem").GetComponent<AudioSource>()) {
            GameObject.Find("EventSystem").GetComponent<AudioSource>().volume *= PlayerPrefs.GetFloat("VolumeModifier");
        }
	}

	void Update()
	{
		if (isRunning && am != null)
        {
			am.cooldown = 1f;
        }

		if (Input.GetMouseButtonDown(0) && isRunning)
		{
			DisplayNextSentence();
		}
	}

	public void StartDialogue(Dialogue dialogue)
	{
		if (isRunning)
        {
			EndDialogue();
        }

		if (GameObject.Find("David Brown")) {
			am.inDialogue = true;
			tempSpeed = GameObject.Find("David Brown").GetComponent<ThirdPersonMovement>().maxSpeed;
			GameObject.Find("David Brown").GetComponent<ThirdPersonMovement>().maxSpeed = 0;
		}
		
		currentDialogue = dialogue;

		animator.SetBool("IsOpen", true);

		isRunning = true;

		lines.Clear();

		foreach (DialogueLine line in dialogue.lines)
		{
			lines.Enqueue(line);
		}

		DisplayNextSentence();
	}

	public void DisplayNextSentence()
	{
		if (isTyping)
        {
			skipDialogue = true;
			return;
        }

		if (lines.Count == 0)
		{
			EndDialogue();
			return;
		}

		DialogueLine line = lines.Dequeue();
		StopAllCoroutines();
		StartCoroutine(TypeSentence(line));
	}

	IEnumerator TypeSentence(DialogueLine line)
	{
		isTyping = true;
		nameText.text = line.name;
		nameText.color = line.nameColor;
		dialogueText.text = "";
		dialogueText.color = line.dialogueColor;

		GameObject.Find("Transition_Overlay").GetComponent<Image>().sprite = line.CG;
		
		/*
		if (GameObject.Find("Transition_Overlay").GetComponent<Image>().sprite != line.CG) {
			GameObject.Find("Transition_Overlay").GetComponent<Animator>().SetTrigger("fadeIn");
			if (line.CG == null) {
				// GameObject.Find("Transition_Overlay").GetComponent<Animator>().SetTrigger("fadeOut");
				GameObject.Find("Transition_Overlay").GetComponent<Image>().sprite = null;
			} else {
				// GameObject.Find("Transition_Overlay").GetComponent<Animator>().SetTrigger("fadeIn");
				GameObject.Find("Transition_Overlay").GetComponent<Image>().sprite = line.CG;
				
			}
		}
		*/

		int ping = 1;
		foreach (char letter in line.dialogue.ToCharArray())
		{
			dialogueText.text += letter;
			if (!skipDialogue) {
				if (ping % 2 == 1) _as.PlayOneShot(blip, 0.08f * PlayerPrefs.GetFloat("VolumeModifier"));
				ping += 1;
				yield return new WaitForSecondsRealtime(0.03f);
			}
		}

		skipDialogue = false;
		isTyping = false;
	}

	void EndDialogue()
	{
		currentDialogue = null;
		isRunning = false;
		if (am != null) am.inDialogue = false;
		animator.SetBool("IsOpen", false);

		if (GameObject.Find("David Brown")) GameObject.Find("David Brown").GetComponent<ThirdPersonMovement>().maxSpeed = tempSpeed;
		if (FindObjectOfType<AgentEnemy>()) FindObjectOfType<AgentEnemy>().enabled = true;

		if (GameObject.Find("Skull Fog") && FindObjectOfType<ActionManager>().evidenceFound > 0 && dialogueText.text.Contains("handle")) {
            StartCoroutine(LoadReaper());
        }

		if (GameObject.Find("ReaperSpawn") && FindObjectOfType<ActionManager>().evidenceFound > 0 && dialogueText.text.Contains("God")) {
            GameObject.Find("ReaperSpawn").GetComponent<ReaperSpawning>().Spawn();
        }	
	}

	IEnumerator LoadReaper() {
        GameObject.Find("Transition_Overlay").GetComponent<Animator>().SetTrigger("fadeIn");
        yield return new WaitForSecondsRealtime(1.5f);
        SceneManager.LoadScene("ReaperBattle_Black");
    }
}
