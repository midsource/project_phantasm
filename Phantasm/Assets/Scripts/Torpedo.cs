﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torpedo : MonoBehaviour
{
    public float damage;
    public float lifetime;
    public Transform player;
    public GameObject hitFX;

    void Start()
    {
        lifetime = 50.0f;
        player = GameObject.FindWithTag("Player").transform;
    }

    void Update()
    {
        lifetime -= Time.deltaTime;
        if (lifetime < 0)
        {
            Destroy(gameObject);
        }

        GetComponent<Rigidbody>().velocity = (player.position + new Vector3(0, 1.2f, 0) - transform.position).normalized * 4;
        transform.LookAt(player.position + new Vector3(0, 1.2f, 0));
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.transform.gameObject.tag == "Player")
        {
            if (hitFX != null) { Instantiate(hitFX, transform.position, Quaternion.identity); }
            col.transform.gameObject.GetComponent<StatusManager>().TakeDamage(damage);
            Destroy(gameObject);
        }
    }
}
