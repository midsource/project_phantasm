﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AnalysisTarget : MonoBehaviour
{
    public Dialogue dialogue;
    public DialogueManager dm;
    public bool available = true;

    void Start()
    {
        dm = FindObjectOfType<DialogueManager>();
        available = true;
    }

    public void Analyze() {
        if (available) {
            FindObjectOfType<ActionManager>().evidenceFound += 1;
            dm.StartDialogue(dialogue);
            available = false;
        } 
    }
}
