﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public string type;
    public Dialogue dialogue;
    public DialogueManager dm;
    public bool active = true;

    public AudioClip clip;

    public GameObject paper;

    void Start()
    {
        dm = FindObjectOfType<DialogueManager>();
    }

    public void Interact()
    {
        if (!active) {
            Debug.Log("Interactable locked");
            return;
        } else {
            Debug.Log("Interaction successful!");
        }

        switch (type)
        {
            case "NPC":
                RunDialogue();
                break;
            case "FOLDER":
                OpenFolder();
                break;
            case "DOOR":
                OpenDoor();
                break;
            default:
                Destroy(gameObject);
                break;
        }
        
    }

    void OpenDoor()
    {
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<Animation>().Play();
        if (GetComponent<AudioSource>())
        {
            GetComponent<AudioSource>().pitch = 1;
            GetComponent<AudioSource>().PlayOneShot(clip, 1f * PlayerPrefs.GetFloat("VolumeModifier"));
        }

        // Invoke("CloseDoor", 2.0f);

        Destroy(this);
    }

    void CloseDoor()
    {
        GetComponent<BoxCollider>().enabled = true;
        GetComponent<Animation>()["DoorOpen"].speed = -1.0f;
        GetComponent<Animation>().Play();
        if (GetComponent<AudioSource>())
        {
            GetComponent<AudioSource>().pitch = -1;
            GetComponent<AudioSource>().PlayOneShot(clip, 1f * PlayerPrefs.GetFloat("VolumeModifier"));
        }
    }

    void OpenFolder()
    {
        paper.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    void RunDialogue()
    {
        if (dm.isRunning)
        {
            // dm.DisplayNextSentence();
        } else
        {
            dm.StartDialogue(dialogue);
        }
        
    }
}
