﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CutsceneDialogueManager : MonoBehaviour
{
	public Text nameText;
	public Text dialogueText;
	public Dialogue currentDialogue;
	public bool isRunning;
	public bool isTyping;
	public bool skipDialogue;

	public AudioSource _as;
	public AudioClip blip;

	private float tempSpeed;

	public Animator animator;

	private Queue<DialogueLine> lines;

	// Use this for initialization
	void Start()
	{
		lines = new Queue<DialogueLine>();
		isTyping = false;
		skipDialogue = false;

		_as = GetComponent<AudioSource>();
		animator = GameObject.Find("DialogueBox").GetComponent<Animator>();

		nameText = GameObject.Find("NameText").GetComponent<Text>();
		dialogueText = GameObject.Find("MessageText").GetComponent<Text>();
	}

	void Update()
	{
		if (Input.GetMouseButtonDown(0) && isRunning)
		{
			DisplayNextSentence();
		}
	}

	public void StartDialogue(Dialogue dialogue)
	{
		if (isRunning)
        {
			EndDialogue();
        }
		
		currentDialogue = dialogue;

		animator.SetBool("IsOpen", true);

		isRunning = true;

		lines.Clear();

		foreach (DialogueLine line in dialogue.lines)
		{
			lines.Enqueue(line);
		}

		DisplayNextSentence();
	}

	public void DisplayNextSentence()
	{
		if (isTyping)
        {
			skipDialogue = true;
			return;
        }

		if (lines.Count == 0)
		{
			EndDialogue();
			return;
		}

		DialogueLine line = lines.Dequeue();
		StopAllCoroutines();
		StartCoroutine(TypeSentence(line));
	}

	IEnumerator TypeSentence(DialogueLine line)
	{
		isTyping = true;
		nameText.text = line.name;
		nameText.color = line.nameColor;
		dialogueText.text = "";
		dialogueText.color = line.dialogueColor;

		GameObject.Find("Transition_Overlay").GetComponent<Image>().sprite = line.CG;
		
		/*
		if (GameObject.Find("Transition_Overlay").GetComponent<Image>().sprite != line.CG) {
			GameObject.Find("Transition_Overlay").GetComponent<Animator>().SetTrigger("fadeIn");
			if (line.CG == null) {
				// GameObject.Find("Transition_Overlay").GetComponent<Animator>().SetTrigger("fadeOut");
				GameObject.Find("Transition_Overlay").GetComponent<Image>().sprite = null;
			} else {
				// GameObject.Find("Transition_Overlay").GetComponent<Animator>().SetTrigger("fadeIn");
				GameObject.Find("Transition_Overlay").GetComponent<Image>().sprite = line.CG;
				
			}
		}
		*/

		int ping = 1;
		foreach (char letter in line.dialogue.ToCharArray())
		{
			dialogueText.text += letter;
			if (!skipDialogue) {
				if (ping % 2 == 1) _as.PlayOneShot(blip, 0.08f * PlayerPrefs.GetFloat("VolumeModifier"));
				ping += 1;
				yield return new WaitForSecondsRealtime(0.03f);
			}
		}

		skipDialogue = false;
		isTyping = false;
	}

	void EndDialogue()
	{
		currentDialogue = null;
		isRunning = false;
		animator.SetBool("IsOpen", false);

        Debug.Log("dialogue ended(?)");
		if (dialogueText.text.Contains("Just... give me time")) {
			StartCoroutine(ReturnToMenu());
		} else {
			Invoke("LoadNextScene", 2f);
		}
	}

    void LoadNextScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

	IEnumerator ReturnToMenu() {
		yield return new WaitForSecondsRealtime(5f);
		SceneManager.LoadScene("MainMenu");
	}

}
