﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class AgentEnemy : MonoBehaviour
{
    // Navigation
    public UnityEngine.AI.NavMeshAgent agent;
    public Transform player;
    public LayerMask groundMask, playerMask;

    // Patrolling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkRange;
    public float chaseDelay;

    // Attacking
    public float cooldown;
    bool attacked;
    public GameObject projectile;
    public GameObject projectile2;
    public Transform firingNode;

    // States
    public float sightRange, attackRange;
    public bool playerDetecting, playerAttacking;
    public Dialogue deathDialogue;
    int shots = 0;

    // Animator
    public Animator anim;

    // Sound
    public AudioSource _as;
    public AudioClip fireClip;

    // -------------------------------------------------
    // STARTER FUNCTIONS
    // -------------------------------------------------

    void Start()
    {
        // player = GameObject.FindWithTag("Player").transform;
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        player = GameObject.FindWithTag("Player").transform;
        Debug.Log(GameObject.FindWithTag("Player"));

        anim = GetComponent<Animator>();
        _as = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (GetComponent<EnemyHP>().currHP < 1) {
            agent.SetDestination(transform.position);
            return; 
        }

        if (player == null)
        {
            player = GameObject.FindWithTag("Player").transform;
        }

        if (chaseDelay > 0.0f) {
            chaseDelay -= Time.deltaTime;
        }

        // Sight and attack range update
        playerDetecting = Physics.CheckSphere(transform.position, sightRange, playerMask);
        playerAttacking = Physics.CheckSphere(transform.position, attackRange, playerMask);

        if (!agent.enabled)
        {
            StartCoroutine(Recover());
        }
        else if (GameObject.Find("David Brown").GetComponent<StatusManager>().playerAlive == true)
        {
            // if (!playerDetecting && !playerAttacking) Patrolling();
            if (playerDetecting && !playerAttacking) { 
                if (!Physics.CheckSphere(transform.position, attackRange + 3f, playerMask)) {
                    ChasePlayer();
                } else {
                    AttackPlayer();
                }   
            };
            if (playerAttacking && playerDetecting) AttackPlayer();
        }
    }

    // -------------------------------------------------
    // MOVEMENT AND AI
    // -------------------------------------------------

    private void Patrolling()
    {
        anim.SetFloat("speed", 1);
        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet)
            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }

    private void SearchWalkPoint()
    {
        anim.SetFloat("speed", 1);
        //Calculate random point in range
        float randomZ = Random.Range(-walkRange, walkRange);
        float randomX = Random.Range(-walkRange, walkRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, groundMask))
            walkPointSet = true;
    }

    private void ChasePlayer()
    {
        anim.SetFloat("speed", 1);
        if (!attacked)
        {
            anim.SetTrigger("fire");
        }
        agent.SetDestination(player.position);
        transform.LookAt(player);
    }

    // -------------------------------------------------
    // ATTACKING AND DAMAGING
    // -------------------------------------------------

    private void AttackPlayer()
    {
        transform.LookAt(player);
        agent.SetDestination(transform.position);

        if (!attacked && shots%5 == 0)
        {
            attacked = true;
            anim.SetTrigger("fire");
            anim.SetFloat("accel", 0.5f);
            anim.SetFloat("speed", 0f);
            Invoke(nameof(ResetAttack), cooldown);
        } else if (!attacked) {
            attacked = true;
            anim.SetTrigger("fire");
            anim.SetFloat("speed", 0f);
            //Make sure enemy doesn't move
            Invoke(nameof(ResetAttack), cooldown);
        }
    }

    public void WhiteFire()
    {
        transform.LookAt(player);
        _as.PlayOneShot(fireClip, PlayerPrefs.GetFloat("VolumeModifier"));

        if (shots%5 != 0) {
            Rigidbody rb = Instantiate(projectile, firingNode.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.velocity = (player.position + new Vector3(0, 1.2f, 0) - firingNode.position).normalized * 120;
            // rb.AddForce(transform.up * 0.1f, ForceMode.Impulse);
            
        } else {
            Rigidbody rb = Instantiate(projectile2, firingNode.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.velocity = (player.position + new Vector3(0, 1.2f, 0) - firingNode.position).normalized * 70;
            anim.SetFloat("accel", 1f);
        }

        
        shots++;
    }

    private void ResetAttack()
    {
        attacked = false;
    }

    public IEnumerator Recover()
    {
        yield return new WaitForSeconds(0.5f);
        GetComponent<Rigidbody>().isKinematic = true;
        agent.enabled = true;
        GetComponent<Animator>().applyRootMotion = true;
    }
}
