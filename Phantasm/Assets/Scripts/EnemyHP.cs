﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class EnemyHP : MonoBehaviour
{
    // Health
    public float maxHP;
    public float currHP;

    // UI
    public GameObject healthUI;
    public Slider healthSlider;

    // Sound
    public AudioSource _as;
    public AudioClip[] hurtClips;

    // Animation
    public Animator anim;
    public int hurtAnims;

    // Start is called before the first frame update
    void Start()
    {
        currHP = maxHP;
        healthSlider.value = currHP / maxHP;

        if (GetComponent<Animator>()) {
            anim = GetComponent<Animator>();
        }

        if (GetComponent<AudioSource>())
            _as = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currHP < 1) { return; }

        // Health values and UI update
        healthSlider.value = currHP / maxHP;
        if (currHP < maxHP)
            healthUI.SetActive(true);

        healthUI.transform.rotation = Camera.main.transform.rotation;
        if (currHP > maxHP)
            currHP = maxHP;
    }

    public void TakeDamage(float damage)
    {
        if (currHP >= 1)
        {
            if (hurtClips.Length > 0)
            {
                _as.PlayOneShot(hurtClips[Random.Range(0, hurtClips.Length)], 0.7f * PlayerPrefs.GetFloat("VolumeModifier"));
            }

            if (hurtAnims > 0)
            {
                anim.SetTrigger("hurt" + Random.Range(1, hurtAnims));
            }

            currHP -= damage;
            Debug.Log(healthSlider.value);
            if (currHP < 1)
            {
                StartCoroutine(Die());
            }
        }
    }

    public IEnumerator Die()
    {
        if (hurtAnims > 0 || gameObject.name.Contains("Leviathan"))
        {
            anim.SetBool("dead", true);
        }

        Destroy(healthUI);
        yield return new WaitForSeconds(2);
        if (FindObjectOfType<AgentEnemy>())
        {
            FindObjectOfType<DialogueManager>().StartDialogue(FindObjectOfType<AgentEnemy>().deathDialogue);
            yield return new WaitForSeconds(4);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        } else if (FindObjectOfType<ReaperAI>())
        {
            yield return new WaitForSeconds(8);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene("Ending");
        } else if (gameObject.name.Contains("Leviathan")) {
            yield return new WaitForSeconds(4);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        } else
        {
            Destroy(gameObject);
        }
        
    }
}
