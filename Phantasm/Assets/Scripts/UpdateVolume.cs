﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateVolume : MonoBehaviour
{
    void Start() {
        PlayerPrefs.SetFloat("VolumeModifier", 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        GameObject.Find("VolumeSlider").GetComponent<Slider>().value = PlayerPrefs.GetFloat("VolumeModifier");
        GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("VolumeModifier");
    }
}
