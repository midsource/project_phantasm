﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfernoDamage : MonoBehaviour
{
    public float damage;
    public float lifetime;
    public AudioSource _as;
    public AudioClip infernoStartClip;

    void Start()
    {
        lifetime = 5.0f;
        damage = 10.0f;
        _as.PlayOneShot(infernoStartClip, 0.7f * PlayerPrefs.GetFloat("VolumeModifier"));
    }

    void Update()
    {
        lifetime -= Time.deltaTime;
        if (lifetime < 0)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            col.gameObject.GetComponent<EnemyHP>().TakeDamage(damage * Time.deltaTime);
        }
    }
}
