﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public void MainMenu()
    {
        StartCoroutine(MainLoad());
    }

    public void Reload()
    {
        StartCoroutine(SelfLoad());
    }

    public IEnumerator MainLoad()
    {
        GameObject.Find("Transition_Overlay").GetComponent<Animator>().SetTrigger("fadeIn");
        yield return new WaitForSecondsRealtime(1.5f);
        Time.timeScale = 1;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        SceneManager.LoadScene("MainMenu");
    }

    public IEnumerator SelfLoad()
    {
        GameObject.Find("Transition_Overlay").GetComponent<Animator>().SetTrigger("fadeIn");
        yield return new WaitForSecondsRealtime(1.5f);
        Time.timeScale = 1;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
