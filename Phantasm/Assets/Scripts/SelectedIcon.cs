﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectedIcon : MonoBehaviour {
    public Sprite[] sprites;
    public Image icon;

    void Start()
    {
        icon = GameObject.Find("SelectedIcon").GetComponent<Image>();
    }

    public void changeIcon(string type)
    {
        switch (type)
        {
            case "Fire":
                icon.sprite = sprites[0];
                break;
            case "Interact":
                icon.sprite = sprites[1];
                break;
            case "Item":
                icon.sprite = sprites[2];
                break;
            case "Analyze":
                icon.sprite = sprites[3];
                break;
            case "Deluge":
                icon.sprite = sprites[4];
                break;
            case "Time Warp":
                icon.sprite = sprites[5];
                break;
            case "Inferno":
                icon.sprite = sprites[6];
                break;
            case "Barrier":
                icon.sprite = sprites[7];
                break;
            case "Venom":
                icon.sprite = sprites[8];
                break;
            default:
                break;
        }
    }
}
