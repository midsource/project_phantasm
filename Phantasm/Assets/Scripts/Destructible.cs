﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{
    public GameObject destructFX;

    public void Destroy()
    {
        if (destructFX != null)
        {
            Instantiate(destructFX, transform.position, Quaternion.identity);
        }
        Destroy(gameObject);
    }
}
