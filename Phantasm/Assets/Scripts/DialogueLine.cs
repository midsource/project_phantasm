﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class DialogueLine : MonoBehaviour
{
    public string name;
    public string dialogue;
    public Sprite CG;

    public Color nameColor = new Color(113/255f, 0f, 0f, 255f);
    public Color dialogueColor = new Color(113/255f, 0f, 0f, 255f);
}
