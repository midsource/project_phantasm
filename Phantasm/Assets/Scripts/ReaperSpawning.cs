﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReaperSpawning : MonoBehaviour
{
    public GameObject reaper;

    public void Spawn()
    {
        reaper.SetActive(true);
    }
}
