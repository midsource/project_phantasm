﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    // Navigation
    public UnityEngine.AI.NavMeshAgent agent;
    public Transform player;
    public LayerMask groundMask, playerMask;

    // Patrolling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkRange;

    // Attacking
    public float cooldown;
    bool attacked;
    public GameObject projectile;
    public Transform firingNode;
    bool stunned;

    // States
    public float sightRange, attackRange;
    public bool playerDetecting, playerAttacking;

    // Animator
    public Animator anim;


    // -------------------------------------------------
    // STARTER FUNCTIONS
    // -------------------------------------------------

    void Start()
    {
        player = GameObject.FindWithTag("Player").transform;
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        stunned = false;
    }

    void Update()
    {
        if (GetComponent<EnemyHP>().currHP < 1 || stunned) { 
            anim.SetFloat("speed", 0);
            if (agent.enabled) {
                agent.SetDestination(player.position);
                agent.enabled = false;
            }
            return; 
        }

        if (player == null)
        {
            player = GameObject.FindWithTag("Player").transform;
        }

        transform.LookAt(player.position);

        // Sight and attack range update
        playerDetecting = Physics.CheckSphere(transform.position, sightRange, playerMask);
        playerAttacking = Physics.CheckSphere(transform.position, attackRange, playerMask);

        if (!agent.enabled && !stunned)
        {
            StartCoroutine(Recover());
        } else
        {
            if (!playerDetecting && !playerAttacking) Patrolling();
            if (playerDetecting && !playerAttacking) ChasePlayer();
            if (playerAttacking && playerDetecting) AttackPlayer();
        }
    }

    // -------------------------------------------------
    // MOVEMENT AND AI
    // -------------------------------------------------

    private void Patrolling()
    {
        anim.SetFloat("speed", 1);
        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet) {
            transform.LookAt(walkPoint);
            agent.SetDestination(walkPoint);
        }

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }

    private void SearchWalkPoint()
    {
        //Calculate random point in range
        anim.SetFloat("speed", 1);
        float randomZ = Random.Range(-walkRange, walkRange);
        float randomX = Random.Range(-walkRange, walkRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, groundMask))
            walkPointSet = true;
    }

    private void ChasePlayer()
    {
        anim.SetFloat("speed", 1);
        agent.SetDestination(player.position);
        transform.LookAt(player);
    }

    // -------------------------------------------------
    // ATTACKING AND DAMAGING
    // -------------------------------------------------

    private void AttackPlayer()
    {
        //Make sure enemy doesn't move
        agent.SetDestination(transform.position);
        anim.SetFloat("speed", 0);
        transform.LookAt(player);

        if (!attacked)
        {
            ///Attack code here
            Rigidbody rb = Instantiate(projectile, firingNode.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 4.8f, ForceMode.Impulse);
            anim.SetTrigger("attack1");
            ///End of attack code

            attacked = true;
            Invoke(nameof(ResetAttack), cooldown);
        }
    }

    public void Stun() {
        stunned = true;
        agent.SetDestination(transform.position);
        agent.enabled = false;
        gameObject.GetComponent<Rigidbody>().isKinematic = false;
        anim.SetFloat("speed", 0);
        Invoke("StunRecover", 5.0f);
    }

    private void ResetAttack()
    {
        attacked = false;
    }

    public void StunRecover() {
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        stunned = false;
        agent.enabled = true;
    }

    public IEnumerator Recover()
    {
        yield return new WaitForSeconds(0.5f);
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        agent.enabled = true;
    }

    public void Die()
    {
        // fucking die
        Destroy(gameObject);
    }
}
