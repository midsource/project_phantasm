﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteTutorial : MonoBehaviour
{
	public Dialogue dialogue;
	private bool triggered;

	void Start()
	{
		triggered = false;
		if (PlayerPrefs.GetString("diedOnce") != "true") {
			StartCoroutine(TriggerDialogue());
		} else {
			if (FindObjectOfType<AgentEnemy>()) FindObjectOfType<AgentEnemy>().enabled = true;
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (!triggered)
		{
			TriggerDialogue();
			triggered = true;
		}

	}

	IEnumerator TriggerDialogue()
	{
		yield return new WaitForSeconds(1f);
		FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
	}
}
