﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class ReaperAI : MonoBehaviour
{
    // Navigation
    public UnityEngine.AI.NavMeshAgent agent;
    public Transform player;
    public LayerMask groundMask, playerMask;

    // Patrolling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkRange;
    public float stopRange;

    // Attacking
    public float cooldown;
    bool attacked;
    public bool damaging;
    public GameObject projectile;
    public Transform firingNode;

    // States
    public float sightRange, attackRange;
    public bool playerDetecting, playerAttacking;
    public enum AttackState { Fidget, ReapingScythe, SweepingStrikes, Execution, SplitOfVengeance, WindsOfDecay };
    public AttackState attackState;

    // Animator
    public Animator anim;

    // Sound
    public AudioSource _as;
    public AudioClip deathClip;
    public AudioClip execClip;
    public AudioClip fidgetClip;
    public AudioClip reapClip;
    public AudioClip spiritClip;
    public AudioClip sweepClip;
    public AudioClip windsClip;
    public AudioClip idleClip;
    public AudioClip moveClip;


    // -------------------------------------------------
    // STARTER FUNCTIONS
    // -------------------------------------------------

    void Start()
    {
        // player = GameObject.FindWithTag("Player").transform;
        damaging = false;
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        player = GameObject.FindWithTag("Player").transform;

        anim = GetComponent<Animator>();
        _as = GetComponent<AudioSource>();
        SwapState();
    }

    void Update()
    {
        if (GetComponent<EnemyHP>().currHP < 1) { return; }

        _as.pitch = Time.timeScale;

        if (player == null)
        {
            player = GameObject.FindWithTag("Player").transform;
        }

        transform.LookAt(player.position - new Vector3(0, 1.8f, 0));

        // Sight and attack range update
        playerDetecting = Physics.CheckSphere(transform.position, sightRange, playerMask);
        playerAttacking = Physics.CheckSphere(transform.position, attackRange, playerMask);

        // Movement/attack state update
        if (!agent.enabled)
        {
            StartCoroutine(Recover());
        }
        else if (GameObject.Find("David Brown").GetComponent<StatusManager>().playerAlive == true)
        {
            if (!playerDetecting && !playerAttacking) Patrolling();
            if (playerDetecting && !playerAttacking) ChasePlayer();
            if (playerAttacking && playerDetecting) AttackPlayer();
        }
    }

    // -------------------------------------------------
    // MOVEMENT AND AI
    // -------------------------------------------------

    private void Patrolling()
    {
        anim.SetFloat("speed", 1);
        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet)
        {
            agent.SetDestination(walkPoint);
            transform.LookAt(walkPoint);
        }

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }

    private void SearchWalkPoint()
    {
        anim.SetFloat("speed", 1);
        //Calculate random point in range
        float randomZ = Random.Range(-walkRange, walkRange);
        float randomX = Random.Range(-walkRange, walkRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, groundMask))
            walkPointSet = true;
    }

    private void ChasePlayer()
    {
        bool playerClose = Physics.CheckSphere(transform.position, stopRange, playerMask);
        if (playerClose)
        {
            anim.SetFloat("speed", 0);
            agent.SetDestination(transform.position);
        } else
        {
            anim.SetFloat("speed", 1);
            agent.SetDestination(player.position);
        }   
    }

    // -------------------------------------------------
    // STATEMANAGER
    // -------------------------------------------------

    private void SwapState()
    {
        int randomNum = Random.Range(1, 7);

        switch (randomNum)
        {
            case 1:
                attackState = AttackState.Fidget;
                attackRange = 10;
                cooldown = 2;
                break;
            case 2:
                attackState = AttackState.ReapingScythe;
                attackRange = 5;
                cooldown = 5;
                break;
            case 3:
                attackState = AttackState.SweepingStrikes;
                attackRange = 5;
                cooldown = 5;
                break;
            case 4:
                attackState = AttackState.Execution;
                attackRange = 5;
                cooldown = 5;
                break;
            case 5:
                attackState = AttackState.SplitOfVengeance;
                attackRange = 5;
                cooldown = 5;
                break;
            case 6:
                attackState = AttackState.WindsOfDecay;
                attackRange = 5;
                cooldown = 5;
                break;
        }
    }

    // -------------------------------------------------
    // ATTACKING AND DAMAGING
    // -------------------------------------------------

    private void AttackPlayer()
    {
        bool playerClose = Physics.CheckSphere(transform.position, stopRange, playerMask);
        if (playerClose)
        {
            anim.SetFloat("speed", 0);
            agent.isStopped = true;
        }
        else
        {
            anim.SetFloat("speed", 1);
            agent.isStopped = false;
            agent.SetDestination(player.position);
        }

        if (!attacked)
        {
            attacked = true;
            // Put the attack functionality here
            switch (attackState)
            {
                case AttackState.Fidget:
                    _as.PlayOneShot(fidgetClip, PlayerPrefs.GetFloat("VolumeModifier"));
                    Debug.Log("Fidget");
                    anim.SetTrigger("fidget");
                    break;
                case AttackState.ReapingScythe:
                    _as.PlayOneShot(reapClip, PlayerPrefs.GetFloat("VolumeModifier"));
                    Debug.Log("Reaping");
                    anim.SetTrigger("reaping");
                    break;
                case AttackState.SweepingStrikes:
                    _as.PlayOneShot(sweepClip, PlayerPrefs.GetFloat("VolumeModifier"));
                    Debug.Log("Sweeping");
                    anim.SetTrigger("sweeping");
                    break;
                case AttackState.Execution:
                    _as.PlayOneShot(execClip, PlayerPrefs.GetFloat("VolumeModifier"));
                    Debug.Log("Execution");
                    anim.SetTrigger("execution");
                    break;
                case AttackState.SplitOfVengeance:
                    _as.PlayOneShot(spiritClip, PlayerPrefs.GetFloat("VolumeModifier"));
                    Debug.Log("Split");
                    anim.SetTrigger("split");
                    agent.speed *= 1.8f;
                    break;
                case AttackState.WindsOfDecay:
                    _as.PlayOneShot(windsClip);
                    Debug.Log("Winds");
                    anim.SetTrigger("winds");
                    agent.speed *= 1.5f;
                    break;
            }

            Invoke(nameof(ResetAttack), cooldown);
        }
    }

    public void ReaperEnableDamage()
    {
        damaging = true;
    }

    public void ReaperDisableDamage()
    {
        damaging = false;
        agent.speed = 7;
    }

    private void ResetAttack()
    {
        attacked = false;
        SwapState();
    }

    public IEnumerator Recover()
    {
        if (GetComponent<EnemyHP>().currHP > 0)
        {
            yield return new WaitForSeconds(0.5f);
            gameObject.GetComponent<Rigidbody>().isKinematic = true;
            agent.enabled = true;
        }
    }
}
