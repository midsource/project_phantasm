﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class LeviathanEnemy : MonoBehaviour
{
    // Navigation
    public Transform player;
    public LayerMask groundMask, playerMask;
    public float turnTime = 5f;
    public Vector3 origin;
    public bool approaching;

    // Attacking
    public float cooldown;
    bool attacked;
    public bool damaging;
    public GameObject projectile;
    public GameObject torpedo;
    public GameObject spawn;

    // Nodes
    public Transform jawAnchor;
    public Transform[] cannonNodes;
    public Transform[] torpedoNodes;

    // Effects
    public GameObject aimPreview;
    public GameObject cylinder;
    public GameObject target;

    // States
    public float highRange, closeRange;
    public bool playerFar, playerClose;
    public enum AttackState { CannonShots, TorpedoStrike, Deluge, SummonTheHorde, Bite };
    public AttackState attackState;
    List<int> lastActions = new List<int>();

    // Animator
    public Animator anim;

    // Sound
    public AudioSource _as;
    public AudioClip idleClip;
    public AudioClip moveClip;
    public AudioClip biteClip;
    public AudioClip broadSide;
    public AudioClip cannonClip;
    public AudioClip torpedoClip;
    public AudioClip delugeClip;
    public AudioClip decksClip;

    void Start()
    {
        damaging = false;
        player = GameObject.FindWithTag("Player").transform;
        approaching = false;

        lastActions.Add(0);
        lastActions.Add(0);


        anim = GetComponent<Animator>();
        _as = GetComponent<AudioSource>();
        origin = transform.position;
        SwapState();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<EnemyHP>().currHP < 1) { return; }

        _as.pitch = Time.timeScale;

        if (player == null)
        {
            player = GameObject.FindWithTag("Player").transform;
        }
        if (approaching)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.position, 8 * Time.deltaTime);
        } else
        {
            transform.position = Vector3.MoveTowards(transform.position, origin, 5* Time.deltaTime);
        }

        // Cylinder tracking
        if (cylinder)
        {
            if (GetComponent<EnemyHP>().currHP < 0.1f) Destroy(cylinder);

            target.transform.position = Vector3.MoveTowards(target.transform.position, player.position + new Vector3(0, 1.2f, 0), 10 * Time.deltaTime);
 
            var offset = cannonNodes[0].position - target.transform.position;
            var scale = new Vector3(0.4f, offset.magnitude / 2.0f, 0.4f);

            cylinder.transform.position = target.transform.position + (offset / 2.0f);
            cylinder.transform.up = offset;
            cylinder.transform.localScale = scale;

            if (Vector3.Distance(target.transform.position, player.position + new Vector3(0, 1.2f, 0)) < 0.1f)
            {
                var cannon = Instantiate(projectile, cannonNodes[0].position, Quaternion.identity);
                cannon.GetComponent<Rigidbody>().velocity = (player.position + new Vector3(0, 1.2f, 0) - cannonNodes[0].position).normalized * 200;
                Destroy(cylinder);
            }
        }

        if (cooldown > 0) cooldown -= Time.deltaTime;

        // Sight and attack range update
        playerFar = Physics.CheckSphere(transform.position, highRange, playerMask);
        playerClose = Physics.CheckSphere(transform.position, closeRange, playerMask);

        if (playerFar) Attacking();
    }

    void Attacking()
    {
        var targetRot = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(player.position - transform.position), turnTime * Time.deltaTime);
        // targetRot = Quaternion.Euler(targetRot.x, Mathf.Clamp(targetRot.y, 60.0f, 120.0f), targetRot.z);
        // targetRot = Quaternion.Euler(targetRot.x, targetRot.y, targetRot.z);
        transform.rotation = targetRot;
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, Mathf.Clamp(transform.rotation.eulerAngles.y, 80, 100), transform.rotation.eulerAngles.z);

        if (cooldown <= 0) SwapState();
    }

    private void SwapState()
    {
        if (playerClose)
        {
            int choice = Random.Range(1, 6);
            if (choice == lastActions[lastActions.Count - 1] || choice == lastActions[lastActions.Count - 2])
            {
                return;
            }
            lastActions.Add(choice);
            
            switch (choice) {
                case 1:
                    _as.PlayOneShot(biteClip);
                    Bite();
                    break;
                case 2:
                    _as.PlayOneShot(delugeClip);
                    Deluge();
                    break;
                case 3:
                    _as.PlayOneShot(torpedoClip);
                    TorpedoStrike();
                    break;
                case 4:
                    _as.PlayOneShot(cannonClip);
                    CannonShots();
                    break;
                case 5:
                    _as.PlayOneShot(decksClip);
                    if (!GameObject.Find("Enemy(Clone)"))
                    {
                        HordeSummon();
                    }
                    break;
                default:
                    Bite();
                    break;
            }
        } else if (playerFar)
        {
            switch (Random.Range(1, 5)) {
                case 1:
                    _as.PlayOneShot(decksClip);
                    if (!GameObject.Find("Enemy(Clone)"))
                    {
                        HordeSummon();
                    }
                    break;
                case 2:
                    _as.PlayOneShot(torpedoClip);
                    TorpedoStrike();
                    break;
                case 3:
                    _as.PlayOneShot(cannonClip);
                    CannonShots();
                    break;
                case 4:
                    _as.PlayOneShot(delugeClip);
                    Deluge();
                    break;
                default:
                    break;
            }
        }
    }

    void CannonShots()
    {
        cooldown = 3;
        if (Vector3.Dot(transform.forward, player.forward) > 0.1)
        {
            anim.SetTrigger("aimLeft");
        } else if (Vector3.Dot(transform.forward, player.forward) < -0.1)
        {
            anim.SetTrigger("aimRight");
        }
            target.transform.position = player.transform.position + new Vector3(Random.Range(-20f, 20f), 0, Random.Range(-20f, 20f));
        var offset = cannonNodes[0].position - target.transform.position;
        var scale = new Vector3(1f, offset.magnitude / 2.0f, 1f);
        var position = target.transform.position + (offset / 2.0f);

        cylinder = Instantiate(aimPreview, position, Quaternion.identity);
        cylinder.transform.up = offset;
        cylinder.transform.localScale = scale;
    }

    void TorpedoStrike()
    {
        cooldown = 7;
        anim.SetTrigger("torpedo");
        foreach (Transform node in torpedoNodes)
        {
            var proj = Instantiate(torpedo, node.position, Quaternion.identity);
        }
    }

    void Deluge()
    {
        anim.SetTrigger("deluge");
        cooldown = 10;
    }

    void DelugeFire()
    {
        Debug.Log("GO FLYING");
        GameObject.Find("Deluge").GetComponent<ParticleSystem>().Play();
        if (Vector3.Dot(transform.forward, player.forward) > -0.3 && Vector3.Dot(transform.forward, player.forward) < 0.3)
        {
            var enemyRB = player.GetComponent<Rigidbody>();
            var moveDir = transform.position - player.transform.position;

            player.GetComponent<ThirdPersonMovement>().disabled = true;
            FindObjectOfType<StatusManager>().TakeDamage(10);
            player.GetComponent<CharacterController>().enabled = false;
            player.GetComponent<Animator>().applyRootMotion = false;

            enemyRB.isKinematic = false;
            enemyRB.useGravity = true;
            enemyRB.AddForce(moveDir.normalized * -25f, ForceMode.Impulse);
            Invoke(nameof(PlayerRecover), 2);
        }
    }

    void HordeSummon()
    {
        cooldown = 7;
        anim.SetTrigger("summon");
        if (!GameObject.Find("Enemy(Clone)"))
        {
            GameObject enemy1 = Instantiate(spawn, new Vector3(Random.Range(-110, -100), 1, Random.Range(53, 130)), Quaternion.identity);
            GameObject enemy2 = Instantiate(spawn, new Vector3(Random.Range(-110, -100), 1, Random.Range(53, 130)), Quaternion.identity);
        }

    }

    void Bite()
    {
        cooldown = 5;
        approaching = true;
        anim.SetTrigger("bite");
    }

    void BiteAttack()
    {
        approaching = false;

        int layerMask = 1 << 9;
        Collider[] hits = Physics.OverlapSphere(jawAnchor.position, 35.0f, layerMask);
        foreach (var hit in hits)
        {
            hit.gameObject.GetComponent<StatusManager>().TakeDamage(15f);
        }
    }

    void PlayerRecover()
    {
        player.GetComponent<ThirdPersonMovement>().disabled = false;
        player.GetComponent<CharacterController>().enabled = true;
        player.GetComponent<Animator>().applyRootMotion = true;
        var enemyRB = player.GetComponent<Rigidbody>();

        enemyRB.isKinematic = true;
        enemyRB.useGravity = false;
    }
}
