﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageDialogue : MonoBehaviour
{
	public Dialogue dialogue;
	private bool triggered;

	void Start()
    {
			Invoke("TriggerDialogue", 1.0f);
    }

	public void TriggerDialogue()
	{
			FindObjectOfType<CutsceneDialogueManager>().StartDialogue(dialogue);
	}

}
