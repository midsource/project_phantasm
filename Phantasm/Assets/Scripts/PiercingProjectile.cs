﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiercingProjectile : MonoBehaviour
{
    public float damage;
    public float lifetime;
    public GameObject hitFX;

    void Start()
    {
        lifetime = 2.0f;
    }

    void Update()
    {
        lifetime -= Time.deltaTime;
        if (lifetime < 0)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (hitFX != null) { Instantiate(hitFX, transform.position, Quaternion.identity); }
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<StatusManager>().TakeDamage(damage);
            Destroy(gameObject);
        }
    }
}
