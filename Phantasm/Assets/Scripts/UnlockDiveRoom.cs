﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockDiveRoom : MonoBehaviour
{
	private bool triggered;

	void Start()
    {
		triggered = false;
    }

	private void OnTriggerEnter(Collider other)
	{
		if (!triggered)
        {
			GameObject.Find("roomDoor2 (1)").GetComponent<Interactable>().active = true;
			triggered = true;
		}
		
	}
}
