﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveManager : MonoBehaviour
{
    StatusManager status;

    void Start() {
        if (FindObjectOfType<StatusManager>()) status = FindObjectOfType<StatusManager>();
    }

    public void SaveGame() {
        SaveSystem.SaveGame(status);
    }

    public void LoadGame() {
        Data data = SaveSystem.LoadGame();
        DontDestroyOnLoad(this.gameObject);
        SceneManager.LoadScene(data.scene);
        StartCoroutine(RealLoad(data));
    }

    IEnumerator RealLoad(Data data) {
        yield return null;
        status = FindObjectOfType<StatusManager>();
        Debug.Log(status != null);
        status.HP = data.HP;
        status.currTime = data.currTime;
        
        FindObjectOfType<ThirdPersonMovement>().disabled = false;
        GameObject.FindWithTag("Player").transform.position = new Vector3(data.position[0], data.position[1], data.position[2]);
        FindObjectOfType<ThirdPersonMovement>().disabled = true;
        Debug.Log("LOADED LET'S GOOO");
    }
}
