﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RingMenu : MonoBehaviour
{
    // Appearance control
    public Ring Data;
    public RingCakePiece RingCakePiecePrefab;
    public float GapWidthDegree = 1f;
    public Action<string> callback;
    protected RingCakePiece[] Pieces;
    protected RingMenu Parent;
    public string Path;

    // Functionality control
    public ActionManager actionManager;
    public SelectedIcon selectedIcon;
    private Text actionText;
    private Text descText;

    // Sound control
    public AudioSource a_s;
    public AudioClip tickClip;
    public AudioClip selectClip;

    void Start()
    {
        actionManager = FindObjectOfType<ActionManager>();
        // actionManager.canMenu = true;
        selectedIcon = FindObjectOfType<SelectedIcon>();
        a_s = GetComponent<AudioSource>();

        var stepLength = 360f / Data.Elements.Length;
        var iconDist = Vector3.Distance(RingCakePiecePrefab.Icon.transform.position, RingCakePiecePrefab.CakePiece.transform.position);

        GameObject.Find("ActionInfo").GetComponent<CanvasGroup>().alpha = 1;
        actionText = GameObject.Find("ActionText").GetComponent<Text>();
        descText = GameObject.Find("DescText").GetComponent<Text>();

        // Position it
        Pieces = new RingCakePiece[Data.Elements.Length];
        if (actionManager.ringRotation != null) {
            transform.localRotation = actionManager.ringRotation;
        }

        for (int i = 0; i < Data.Elements.Length; i++)
        {
            Pieces[i] = Instantiate(RingCakePiecePrefab, transform);
            // Set root element
            Pieces[i].transform.localPosition = Vector3.zero;
            Pieces[i].transform.localRotation = Quaternion.identity;

            // Set cake piece
            Pieces[i].CakePiece.fillAmount = 1f / Data.Elements.Length - GapWidthDegree / 360f;
            Pieces[i].CakePiece.transform.localPosition = Vector3.zero;
            Pieces[i].CakePiece.transform.localRotation = Quaternion.Euler(0, 0, -stepLength / 2f + GapWidthDegree / 2f + i * stepLength);
            Pieces[i].CakePiece.color = new Color(1f, 1f, 1f, 0);

            // Set icon
            Pieces[i].Icon.transform.localPosition = Pieces[i].CakePiece.transform.localPosition + Quaternion.AngleAxis(i * stepLength, Vector3.forward) * Vector3.up * iconDist;
            Pieces[i].Icon.transform.rotation = Quaternion.Euler(0, 0, -stepLength / 2f + GapWidthDegree / 2f + (i * stepLength) + stepLength/2) * transform.localRotation;
            Pieces[i].Icon.sprite = Data.Elements[i].Icon;
        }
    }

    private void Update()
    {
        var stepLength = 360f / Data.Elements.Length;
        var rotFactor = transform.rotation;

        int activeElement = 0;
        float maxValue = 0.0f;
        for (int i = 0; i < Data.Elements.Length; i++)
        {
            if (Pieces[i].Icon.transform.position.y > maxValue)
            {
                maxValue = Pieces[i].Icon.transform.position.y;
                activeElement = i;
                actionText.text = Data.Elements[activeElement].Name;
                SwitchDescription(activeElement);
            }
        }
        

        /*
        for (int i = 0; i < Data.Elements.Length; i++)
        {
            if (i == activeElement)
            {
                Pieces[i].CakePiece.color = new Color(1f, 1f, 1f, 0.75f);
            }
            else
            {
                Pieces[i].CakePiece.color = new Color(1f, 1f, 1f, 0.2f);
            }
        }
        */
        
        // Rotating revolver
        if (Input.GetAxis("Mouse ScrollWheel") != 0f) // forward
        {
            transform.localRotation *= Quaternion.Euler(0, 0, Input.GetAxis("Mouse ScrollWheel") * 600f);
            if (!a_s.isPlaying)
                a_s.PlayOneShot(tickClip, 0.5f * PlayerPrefs.GetFloat("VolumeModifier"));
        }

            // Selecting action and proceeding in the wheel
            if (Input.GetMouseButtonDown(0))
        {
            var path = Path + "/" + Data.Elements[activeElement].Name;
            selectedIcon.changeIcon(Data.Elements[activeElement].Name);
            actionManager.slottedAction = Data.Elements[activeElement].Name;

            // Play sound
            if (!a_s.isPlaying)
                a_s.PlayOneShot(selectClip, 0.5f * PlayerPrefs.GetFloat("VolumeModifier"));

            
            if (Data.Elements[activeElement].NextRing != null)
            { // If there is a next ring (specials/inventory)
                var newSubRing = Instantiate(gameObject, transform.parent).GetComponent<RingMenu>();
                newSubRing.Parent = this;
                for (var j = 0; j < newSubRing.transform.childCount; j++)
                    Destroy(newSubRing.transform.GetChild(j).gameObject);
                newSubRing.Data = Data.Elements[activeElement].NextRing;
                newSubRing.Path = path;
                newSubRing.callback = callback;
                newSubRing.Data.LastRing = GetComponent<RingMenu>().Data;
            }
            else
            { // If there is no next ring (everything else)
                actionManager.canMenu = true;
                GameObject.Find("ActionInfo").GetComponent<CanvasGroup>().alpha = 0;
                switch (actionManager.slottedAction)
                {
                    case "Fire":
                        actionManager.UseAction("Fire");
                        break;
                    case "Time Warp":
                        actionManager.UseAction("Time Warp");
                        break;
                    case "Deluge":
                        actionManager.UseAction("Deluge");
                        break;
                    case "Inferno":
                        actionManager.UseAction("Inferno");
                        break;
                    case "Barrier":
                        actionManager.UseAction("Barrier");
                        break;
                    case "Venom":
                        actionManager.UseAction("Venom");
                        break;
                    default:
                        actionManager.ReturnTime();
                        break;
                }
                    
                callback?.Invoke(path);
            }
            gameObject.SetActive(false);
        }

        // Going back to the previous ring
        if (Input.GetMouseButtonDown(1))
        {
            var path = Path + "/" + Data.Elements[activeElement].Name;

            // Play sound
            if (!a_s.isPlaying)
                a_s.PlayOneShot(selectClip, 0.5f * PlayerPrefs.GetFloat("VolumeModifier"));

            
            if (Data.LastRing != null)
            { // If there is a previous ring (specials/inventory)
                var newSubRing = Instantiate(gameObject, transform.parent).GetComponent<RingMenu>();
                newSubRing.Parent = this;
                for (var j = 0; j < newSubRing.transform.childCount; j++)
                    Destroy(newSubRing.transform.GetChild(j).gameObject);
                newSubRing.Data = Data.LastRing;
                newSubRing.Path = path;
                newSubRing.callback = callback;
                gameObject.SetActive(false);
            }
            
        }
    }

    private void SwitchDescription(int activeElement) {
        switch(Data.Elements[activeElement].Name) {
            case "Fire":
                descText.text = "Aim and fire at a target.";
                break;
            case "Interact":
                descText.text = "Activate or speak to a target.";
                break;
            case "Special":
                descText.text = "Select a special ability.";
                break;
            case "Analyze":
                descText.text = "Analyze an object or gather evidence.";
                break;
            case "Item":
                descText.text = "Select and use an item.";
                break;
            case "Cancel":
                descText.text = "Exit the menu.";
                break;
            case "Deluge":
                descText.text = "Blow an enemy away.";
                break;
            case "Time Warp":
                descText.text = "Teleport to another location.";
                break;
            case "Inferno":
                descText.text = "Summon a ring of fire.";
                break;
            case "Barrier":
                descText.text = "Create a barrier cube.";
                break;
            case "Venom":
                descText.text = "Fire off a venom shot.";
                break;
            default: 
                descText.text = "Idk";
                break;
        }
        
    }

    private float NormalizeAngle(float a) => (a + 360f) % 360f;
}
