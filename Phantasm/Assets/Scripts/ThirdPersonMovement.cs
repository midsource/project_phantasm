﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonMovement : MonoBehaviour
{
    // Main references
    public CharacterController controller;
    private ActionManager am;
    public Animator anim;
    public Transform cam;

    // Movement control
    public float maxSpeed = 6f;
    public float currSpeed = 0f;
    private float gravity = -9.81f;
    public float jumpForce = 1.5f;
    Vector3 velocity;
    public float turnTime = 0.1f;
    float turnVelocity;

    // Attack control
    public bool disabled;
    public bool paused;
    public bool aiming;

    // Sound control
    public AudioSource a_s;
    public AudioClip walkingClip;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        a_s = GetComponent<AudioSource>();
        am = FindObjectOfType<ActionManager>();
        cam = GameObject.FindWithTag("MainCamera").transform;
        aiming = false;

        disabled = false;
        paused = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;   // Keeps cursor confined to center
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && !paused && FindObjectOfType<StatusManager>().playerAlive == true && !am.slowed) {
            Time.timeScale = 0;
            disabled = true;
            GameObject.Find("PauseMenu").GetComponent<Animator>().SetBool("isDead", true);
            GameObject.Find("PauseMenu").GetComponent<CanvasGroup>().interactable = true;
            GameObject.Find("PauseMenu").GetComponent<CanvasGroup>().blocksRaycasts = true;
            StartCoroutine(Pause());
        } else if (Input.GetKey(KeyCode.Escape) && paused && FindObjectOfType<StatusManager>().playerAlive == true && !am.slowed) {
            Debug.Log("waaaaa");
            GameObject.Find("PauseMenu").GetComponent<Animator>().SetBool("isDead", false);
            GameObject.Find("PauseMenu").GetComponent<CanvasGroup>().interactable = false;
            GameObject.Find("PauseMenu").GetComponent<CanvasGroup>().blocksRaycasts = false;
            StartCoroutine(Unpause());
        }

        if (disabled) { return; }

        if (Input.GetKey(KeyCode.Tab)) {
            FindObjectOfType<SaveManager>().SaveGame();
        }

        float hor = Input.GetAxisRaw("Horizontal");
        float vert = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(hor, 0f, vert).normalized;

        // Movement vertically
        if (controller.isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
            anim.SetBool("jump", false);
        }

        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        // Stop movement if in dialogue
        if (am.inDialogue)
        {
            anim.SetFloat("speed", 0);
            return;
        }

        if (Input.GetButtonDown("Jump") && controller.isGrounded)
        {
            anim.SetBool("jump", true);
        }

        if (controller.isGrounded) {
            anim.SetBool("land", true);
        } else {
            anim.SetBool("land", false);
        }

        // Free cursor for playtesting
        if (Input.GetKey(KeyCode.Equals))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        // Movement horizontally
        if (Input.GetKey (KeyCode.LeftShift))
        {
            currSpeed = maxSpeed;
        } else
        {
            currSpeed = maxSpeed;
        }

        float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
        float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnVelocity, turnTime);

        // Move the character when input is detected
        if (direction.magnitude >= 0.1f)
        {
            anim.SetFloat("speed", currSpeed*1.6f);
            if (!a_s.isPlaying && controller.isGrounded && maxSpeed > 0.1)
            {
                a_s.PlayOneShot(walkingClip, PlayerPrefs.GetFloat("VolumeModifier"));
            }

            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            controller.Move(moveDir.normalized * currSpeed * Time.deltaTime);
        }
        else
        {
            if (a_s.isPlaying)
            {
                a_s.Stop();
            }
            anim.SetFloat("speed", 0);
        }

        // Rotate the character appropriately
        if (am.slowed || am.quickActivation)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, cam.eulerAngles.y, 0), turnTime * Time.fixedDeltaTime);
        }
        else if (direction.magnitude >= 0.1f)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, targetAngle, 0), turnTime * Time.fixedDeltaTime);
        }
    }

    public void Aimed()
    {
        anim.speed = 0;
        am.Aimed();
    }

    IEnumerator Pause() {
        yield return new WaitForSecondsRealtime(1f);
        paused = true;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    IEnumerator Unpause() {
        yield return new WaitForSecondsRealtime(1f);
        paused = false;
        Time.timeScale = 1;
        disabled = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked; 
        Debug.Log("I'm a stinky little baby");
    }

    public void JumpForce()
    {
        velocity.y = Mathf.Sqrt(jumpForce * -2f * gravity);
    }
}
