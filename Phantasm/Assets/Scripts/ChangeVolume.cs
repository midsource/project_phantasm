﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeVolume : MonoBehaviour
{
    public void VolumeChange() {
        PlayerPrefs.SetFloat("VolumeModifier", GameObject.Find("VolumeSlider").GetComponent<Slider>().value);
    }
}
