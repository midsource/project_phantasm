﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScytheDamage : MonoBehaviour
{
    // Damage dealt
    public float damage;
    public float cooldown;

    // SFX
    public AudioSource _as;
    public AudioClip damageClip;

    void Start()
    {
        cooldown = 0.1f;
    }

    void Update()
    {
        if (cooldown > 0)
        {
            cooldown -= Time.deltaTime;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.transform.gameObject.tag == "Player" && cooldown <= 0.01f && FindObjectOfType<ReaperAI>().damaging == true)
        {
            col.transform.gameObject.GetComponent<StatusManager>().TakeDamage(damage);
            _as.PlayOneShot(damageClip, PlayerPrefs.GetFloat("VolumeModifier"));
            cooldown = 0.1f;
        }
    }

}
