﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StatusManager : MonoBehaviour
{
    public float HP;
    public float maxHP;
    public bool playerAlive;

    public float currTime;
    public float maxTime;
    public float invulTime;

    // UI
    public Slider healthSlider;
    public Transform clockHand;
    public Transform clockHand2;

    // Animation
    public Animator anim;
    public ThirdPersonMovement controller;

    // Start is called before the first frame update
    void Start()
    {
        playerAlive = true;
        maxHP = 100;
        HP = maxHP;
        currTime = 0;
        maxTime = 720;

        healthSlider = GameObject.Find("HealthSlider").GetComponent<Slider>();
        clockHand = GameObject.Find("ClockHand").transform;
        clockHand2 = GameObject.Find("ClockHand2").transform;
        anim = GetComponent<Animator>();
        controller = GameObject.FindWithTag("Player").GetComponent<ThirdPersonMovement>();
    }

    void Update()
    {
        // Health values and UI update
        healthSlider.value = HP / maxHP;

        if (HP > maxHP)
            HP = maxHP;

        if (invulTime > 0) invulTime -= Time.deltaTime;

        currTime += Time.deltaTime;
        clockHand.eulerAngles = new Vector3(0, 0, -(currTime/ maxTime) * 360);
        clockHand2.eulerAngles = new Vector3(0, 0, -(currTime / 60) * 360);

        if (currTime > maxTime) Die();
    }

    public void TakeDamage(float damage)
    {   
        if (invulTime > 0) { return; }

        HP -= damage;
        
        if (FindObjectOfType<ActionManager>().slowed != true) {
            anim.SetTrigger("hit");
            StartCoroutine(Stun());
        }

        if (HP < 1)
            {
                StartCoroutine(Die());
            }
    }
    
    public IEnumerator Stun()
    {
        if (controller.maxSpeed == 0f) { controller.maxSpeed = 6f; }
        float tempSpeed = controller.maxSpeed;
        controller.maxSpeed = 0f;
        yield return new WaitForSecondsRealtime(0.5f);
        controller.maxSpeed = tempSpeed;
        
    }

    public IEnumerator Die()
    {
        // fucking die
        playerAlive = false;
        anim.SetBool("dead", true);
        gameObject.GetComponent<ThirdPersonMovement>().disabled = true;
        PlayerPrefs.SetString("diedOnce", "true");

        Time.timeScale = 0.2f;
        yield return new WaitForSeconds(1f);
        GameObject.Find("GameOver").GetComponent<Animator>().SetTrigger("isDead");
        GameObject.Find("GameOver").GetComponent<CanvasGroup>().interactable = true;
        GameObject.Find("GameOver").GetComponent<CanvasGroup>().blocksRaycasts = true;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0.02f;
    }
}
