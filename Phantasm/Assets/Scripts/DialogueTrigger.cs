﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
	public Dialogue dialogue;
	private bool triggered;

	void Start()
    {
		triggered = false;
    }

	private void OnTriggerEnter(Collider other)
	{
		if (!triggered)
        {
			TriggerDialogue();
			triggered = true;
		}
		
	}

	public void TriggerDialogue()
	{
		FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
		Destroy(gameObject);
	}

}
